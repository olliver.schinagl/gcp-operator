package client

import (
	"k8s.io/client-go/kubernetes"
	"k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/watch"
	"k8s.io/client-go/rest"
)

func NewInClusterClient() (*kubernetes.Clientset, error) {
	cfg, err := rest.InClusterConfig()
	if err != nil {
		return nil, err
	}

	return kubernetes.NewForConfigOrDie(cfg), nil
}

func Subscribe(cli *kubernetes.Clientset, namespace string) (watch.Interface, error) {

	return cli.EventsV1beta1().Events(namespace).Watch(v1.ListOptions{})
}
