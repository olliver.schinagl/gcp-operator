package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

type GCPCloudSQLInstance struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   GCPCloudSQLInstanceSpec   `json:"spec"`
	Status GCPCloudSQLInstanceStatus `json:"status"`
}


type GCPCloudSQLInstanceSpec struct {
	// all cloudsql instance configuration properties go here
}

type GCPCloudSQLInstanceStatus struct {
	// all back reported status goes here
}


